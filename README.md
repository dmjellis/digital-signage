# README #

This is a simple digital signage prototype built for the Raspberry Pi with the content management happening in WordPress. It is a beta system and still a work in progress - ** use at your own risk! **.

# Overview #

The signage system consists of two main parts:

* A RPi connected to a screen which runs a fullscreen browser and fetches content from the web at regular intervals
* A content source - in our case WordPress: but could be any web page. We used the excellent [Reveal.js](http://lab.hakim.se/reveal-js/#/) which is an HTML presentation framework. 

#### Raspberry Pi with connected display ####

We used a Raspberry Pi 2 Model B running [Raspbian Pixel](https://www.raspberrypi.org/downloads/raspbian/). The setup on the Pi is relatively basic, but we used Pixel because it ships with Chromium - in our tests we found this was much easier to configure to run in fullscreen / kiosk mode. Other browsers are available.

For display, any HDMI screen will do. We used a [Digihome 287 43"](https://www.tesco.com/direct/digihome-287-full-hd-43-inch-led-tv-with-freeview-hd/180-1387.prd) from Tesco. It's really big, incredibly cheap (£200) and was recommended to us by a major national museum who have been using it on-gallery with good results. The picture quality is reasonable rather than excellent, but at that price we found it a good choice.

Preventing the screen from sleeping is important for this development, so whatever you use screen-wise, make sure you can keep it awake for long periods of time!

#### Content source, in our case WordPress ####

We used WordPress because our clients are already running it for their websites: they are familiar with its user-friendly interface and we have some kinds of content which already exists within the website which we want to pull down into the signage. An example of this might be the next public event, opening times, or current exhibition details.

# Getting it set up #

This happens in two parts:

## Raspberry Pi setup ##

Do the following:

### Install Raspbian ###

### Update ###

### Install Apache and PHP ###

### Add the files from the /rpi folder to your web directory ###

### Setup the Pi to boot into Chromium running fullscreen ###

We used the following but there may be some variation depending on your setup

```

sudo nano ~/.config/lxsession/LXDE-pi/autostart

#@lxpanel --profile LXDE-pi
#@pcmanfm --desktop --profile LXDE-pi
#@xscreensaver -no-splash
#@point-rpi

@lxpanel --profile LXDE-pi
@pcmanfm --desktop --profile LXDE-pi
# @xscreensaver -no-splash

# Disable power saving shizzle
@xset s off
@xset -dpms
@xset s noblank

# Stop error messages
@sed -i 's/"exited_cleanly": false/"exited_cleanly": true/' ~/.config/chromium/Default/Preferences

# Display Chromium in kiosk mode, with our URL
@chromium-browser --noerrdialogs --kiosk http://localhost --incognito

```

### Change the default password of the Pi ###

### Disable screen blanking ###

We struggled a lot with this (and clearly from Googling it, so does everyone else..) - but in the end found that installing the screensaver and setting the timeout to maximum was the only way. See [this link](http://raspberrypi.stackexchange.com/questions/38515/auto-start-chromium-on-raspbian-jessie-11-2015) for more.

## WordPress setup ##

This will vary depending on your site. We use ACF and so have developed some modules for this so that each "slide" can be easily added and content dropped in. 

Broadly though, do this:

### Setup a Custom Post Type ###

We used "signage" but again, do what you need for your setup.

### Setup the files within your theme ### 

You need the single page to correctly display your slides. So for us this file is single-signage.php but may vary for you. You will need to include the relevant files within your theme (so all the dependent files from reveal.js including js, css, etc. The end result is that your slides should look and work ok when you preview you them from WordPress.

### Publish your page ###

Publish the page and once everything is working ok, make a note of the URL. 

## Amend the rpi/index.php script ##

In future versions, we'll have a config file, but for now:

* Paste in the URL you copied above as the $contenturl variable within the index.php file which is now on your Pi. 
* Change lines 86 and 94 as commented in the source to match your content root

Now check your Pi by opening up Chromium and browsing to http://localhost. If things have worked, you'll find the remote page is pulled in, cached and then displayed. 

Reboot, and when it works go for a pint. If it doesn't work, go for two pints and come back to it later.


### Who do I talk to? ###

* Twitter: [m1ke_ellis](http://twitter.com/m1ke_ellis)
* Contact form: [https://thirty8.co.uk/say-hello/](https://thirty8.co.uk/say-hello/)