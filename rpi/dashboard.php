<?php
// Start the session
session_start();

$userisloggedin = false;

			          
if($_SESSION['amdashboard'] == 'loggedin')
{
	$userisloggedin = true;
}
			            


?>

<!doctype html>
<html lang="en">

	<head>
		<meta charset="utf-8">

		<title>Digital Signage - dashboard</title>
		
		<meta name="description" content="Signage for American Museum">
		<meta name="author" content="Mike Ellis">

		<meta name="apple-mobile-web-app-capable" content="yes" />
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />

		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, minimal-ui">

		<link rel="stylesheet" href="css/theme/americanmuseum.css" id="theme">

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

		<!--[if lt IE 9]>
		<script src="lib/js/html5shiv.js"></script>
		<![endif]-->

		<style>
			
		.wrapper
		{
			color:#ffffff;			
		}
			
		.dashboard
		{
			padding-left:30px;
			margin-left:auto;
			margin-right: auto;
			width:70%;			
		}	

		.dashboard a
		{
			color:#ffffff;
		}

		input[type=text], input[type=password], select 
		{
		    width: 100%;
		    padding: 12px 20px;
		    margin: 8px 0;
		    display: inline-block;
		    border: 1px solid #ccc;
		    border-radius: 4px;
		    box-sizing: border-box;
		}
		
		input[type=submit] {
		    width: 50%;
		    background-color: #7b0606;
		    color: white;
		    padding: 14px 20px;
		    margin: 8px 0;
		    border: none;
		    border-radius: 4px;
		    cursor: pointer;
		}
		
		input[type=submit]:hover 
		{
		    background-color: #940909;
		}
		
		.pwdform 
		{
			width:300px;
		    border-radius: 5px;
		    padding: 20px;
		}
			
		.logoutlink
		{
			text-align: right;
		}	
			
		</style>


	</head>


	<body>

		<div class="wrapper">

			<div class="logo">
				
				<a href="dashboard.php">
				<img src="cacheimages/logo.png" alt="AM logo" />
				</a>
				
			</div>

			<div class="dashboard">
				
			<?php
			$user = $_POST['user'];
			$pass = $_POST['pass'];
									
			if($user == "admin"
			&& $pass == "admin")
			{
				$userisloggedin = true;	
				$_SESSION['amdashboard'] = 'loggedin';							
			}
			?>		
			
			
			<?php 
				if($userisloggedin == true)
				{
			?>

				<h2>Admin dashboard</h2>
				<a name="top"></a>
				
				<p class="logoutlink"><a href="logout.php">Logout</a></p>
				
				<ul>
					<li><a href="#docs">Documentation</li>
					<li><a href="#log">Logfile</a></li>
					<li><a href="#powerdown">Shutdown the Pi</a></li>
				</ul>
				
				<hr />
				
				<h3>Documentation</h3>
				<a name="docs"></a>	
				
				<p>This digital signage system has been developed by <a href="http://thirty8.co.uk" target="_blank">Thirty8 Digital Ltd.</a> For all support questions, please drop a line to <a href="mailto:hello@thirty8.co.uk?subject=Digital Signage">hello@thirty8.co.uk</a> or ring 0800 808 54 38.</p>
				
				<h4>System overview</h4>
				
				<p>
					The system is based around two main parts - a Raspberry Pi computer which powers the signage screen and a content source, which is the main website, built in WordPress. </p>
					
				<p>
					The Raspberry Pi hardware (Raspberry Pi 2 Model B) is designed to operate with low/no maintenance. When powered up, the device should automatically load Chromium in full screen. There will then be a short delay while the system loads, and after this it will display the slides for the chosen digital display. If the Pi is powered down at any time, simply reconnect the power display and it should reboot as normal and continue normal operation. </p>
					
				<p>
					Where possible, try to avoid powering the Pi down by unplugging it: this can cause corruption of the SD card which contains all the necessary software on the device. Instead, use the <a href="#powerdown">link on this page</a> to shutdown the Pi. 
				</p>
				
				<p>If the Pi fails to boot up or consistently shows an error when doing so, then please contact Thirty8 Digital in the first instance. It it possible that you may need to replace the SD card with the replacement backup we have provided. To do this simply power the Pi down, locate the SD card in the side, press it to release and then replace the card with the new one.</p>					
				<p>
					
										
				</p>
				
				<p></p>			


				<hr />

				<h3>Log file</h3>
				<a name="log"></a>
				
				<?php
				
					$logcontents = file_get_contents('log.htm');
					
					$loglines = explode("<br/>",$logcontents);
					
					foreach($loglines as $logline)
					{
						if(strpos($logline,'fetched ok'))
						{
							$okfetch++;
						}

						if(strpos($logline,'Cached'))
						{
							$okfetch++;
						}
						
					}
					
					echo '<p>Log shows that the signage has been fetched successfully ' . $okfetch . ' times</p>';
					
				?>

				<h3>System shutdown</h3>
				<a name="powerdown"></a>
				
				<!--

from https://www.raspberrypi.org/forums/viewtopic.php?t=58802

sudo apt-get update && sudo apt-get install lighttpd php5-cgi
sudo lighty-enable-mod fastcgi fastcgi-php
sudo service lighttpd force-reload
sudo visudo
   # Add the following line below "pi ALL etc." and exit the visudo editor:
   www-data ALL = NOPASSWD: /sbin/shutdown
sudo nano /var/www/index.html
   # Absolute minimum contents of the index.html file:
   <html><a href="shutdown.php">Shutdown NOW</a></html>
sudo nano /var/www/shutdown.php
   # Absolute minimum contents of the shutdown.php file:
   <?php system('sudo /sbin/shutdown -h now'); ?>
					
					
				-->


			
			<?php

				}	
				else
				{
			    if(isset($_POST))
			    {?>
			
				<h2>Please login</h2>
			
					<div class="pwdform">
			
		            <form method="POST" action="dashboard.php">
		            Username: 
		            <input type="text" name="user"></input><br/>
		            Password: 
		            <input type="password" name="pass"></input><br/>
		            <input type="submit" name="submit" value="Login &raquo;"></input>
		            </form>
		            
		            </div>		            

			    <?}					
				}	
				
			?>
				
				
				
			</div>

		</div>



	</body>
</html>