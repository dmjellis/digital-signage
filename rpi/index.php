<html>
	
	<head>
		
		<title>Thirty8 Digital signage v0.1</title>
		
		<meta http-equiv="refresh" content="600" />
		
		<style>
			
			body
			{
				background-color: #222;
			}
			
			#remotecontent
			{
				width:100%;
				height:95%;
				border:0px;
			}
			
		</style>
	
	
	</head>
	
	
<body>	
		


<?php
	
include("includes/simple_html_dom.php");	
	

$fetch = false;
$html = '';
$fetchtimestamp = '';

$fetchtimestamp = $date = date('h:i:s a d/m/Y ', time());

$contenturl = '*** Your URL in here ***';


// break it deliberately
//$contenturl = 'fnag';


if(!$data = @file_get_contents($contenturl))
{

	// get content from local file because remote access / site is borked		
	$html = file_get_contents("cached.htm");

	// Write to log
	
	$log = '<br />Cached content used at ' . $fetchtimestamp;
	file_put_contents('log.htm', $log, FILE_APPEND);

	
	// set iframe src to the cached version
	$pageurl = 'cached.htm';
	
}
else
{
	$fetch = true;
		
	// get content from remote file
	$html = file_get_contents($contenturl);	
	
	$cachedhtml = $html;
	
	$htmlcode = file_get_html($contenturl);	

	foreach($htmlcode->find('img') as $img)
	{
			    
	    $remoteimgpath = $img->src;
	    
	    $pathinfo = pathinfo($remoteimgpath);
	    
	    // Change below line to match your live content source
	    $imagereplace = preg_replace('/https:\/\/americanmuseum.org\/wp-content\/uploads\/.*\/(.*)/', 'cacheimages/' . $pathinfo['basename'] ,$remoteimgpath);
	    
	    $cachedhtml = str_replace($remoteimgpath, $imagereplace, $cachedhtml);	 
	    	       
	}
	
	// Get rid of remote path - change for your content source
	
	$cachedhtml = str_replace('https://americanmuseum.org/wp-content/themes/americanmuseum/signage/','',$cachedhtml);
	
	$cachedhtml = str_replace('images/logo.png','cacheimages/logo.png',$cachedhtml);

	// Write content to cache file
	
	$fp = fopen("cached.htm", 'w');
	fwrite($fp, $cachedhtml);
	fclose($fp);
	
	// Write to log
	
	$log = '<br/>Content fetched ok at ' . $fetchtimestamp;
	file_put_contents('log.htm', $log, FILE_APPEND);


	// Find all the images
	
	foreach($htmlcode->find('img') as $img)
	{
	    
	    $remoteimgpath = $img->src;
	    
	    $pathinfo = pathinfo($remoteimgpath);
	    
	    
	    $localfilename = "cacheimages/" . $pathinfo['basename'];
	
		// Save to images folder
		
		file_put_contents($localfilename, file_get_contents($remoteimgpath));    
		    
	    $outputhtml = str_replace($remoteimgpath,'cacheimages/' . $pathinfo['basename'],$outputhtml);
	       
	}

	// set iframe src to the live version
	$pageurl = $contenturl;
	
}

	if($fetch==true)
	{
		$fetchtext = "Fetched ok at " . $fetchtimestamp;;
	}	
	else
	{
		$fetchtext = "Cached";
	}

?>


<div style="text-align:right; font-size:12px; color:#ffffff;"><?php echo $fetchtext;?></div>

<iframe id="remotecontent" src="<?php echo $pageurl;?>"></iframe>

</body>

</html>
